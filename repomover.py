import requests
import click
import os

class GitlabApi: 
    def __init__(self):
        token = os.environ.get('GITLAB_TOKEN')
        base = os.environ.get('GITLAB_HOST')
        if not token:
            raise Exception("Missing Gitlab auth token")
        if not base:
            raise Exception("Missing Gitlab hostname")
        self.headers = {
            'Authorization': f"Bearer {token}"
        }
        self.base = base + 'api/v4'
    
    def get(self, path, data=None):
        headers = self.headers
        r = requests.get(self.base + path, params=data, headers=headers)
        result = r.json()
        next_header = r.links.get('next')
        if next_header:
            next_url = next_header['url']
        else:
            next_url = None
        return result, next_url

    def get_page(self, url):
        headers = self.headers
        r = requests.get(url, headers=headers)
        result = r.json()
        next_header = r.links.get('next')
        if next_header:
            next_url = next_header['url']
        else:
            next_url = None
        return result, next_url

    def post(self, path, data=None):
        headers = self.headers
        headers['Content-Type'] = 'application/json'
        r = requests.post(self.base + path, json=data, headers=headers)
        result = r.json()
        return result


api = GitlabApi()

@click.group()
def cli():
    pass

@cli.command()
@click.argument('storage')
def get_repos(storage):
    payload = {
        'repository_storage': storage,
        'per_page': 50
    }
    projects, next_page = api.get('/projects', payload)
    for project in projects:
        print(f"{project['id']}\t{project['name_with_namespace']}")
    while next_page:
        projects, next_page = api.get_page(next_page)
        for project in projects:
            print(f"{project['id']}\t{project['name_with_namespace']}")

@cli.command()
@click.argument('repolist')
@click.argument('new_storage')
def move_repos(repolist, new_storage):
    with open(repolist, 'r') as f:
        for line in f.readlines():
            id = line.split("\t")[0]
            payload = {
                'destination_storage_name': new_storage
            }
            rep = api.post(f'/projects/{id}/repository_storage_moves', payload)
            print(rep)

if __name__ == "__main__":
    cli()
